# Generated by Django 3.1.1 on 2020-09-15 22:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('razon_social', models.CharField(max_length=50)),
                ('direccion', models.CharField(max_length=50)),
                ('rfc', models.CharField(max_length=50)),
                ('telefono_oficial', models.CharField(max_length=50)),
                ('telefono_contacto', models.CharField(max_length=50)),
            ],
        ),
    ]
