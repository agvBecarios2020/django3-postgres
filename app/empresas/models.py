from django.db import models

# Create your models here.


class Empresa(models.Model):
    nombre = models.CharField(max_length=50)
    razon_social = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    rfc = models.CharField(max_length=50)
    telefono_oficial = models.CharField(max_length=50)
    telefono_contacto = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre

class Business(models.Model):
    nombre = models.CharField(max_length=50)
    razon_social = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    rfc = models.CharField(max_length=50)
    telefono_oficial = models.CharField(max_length=50)
    telefono_contacto = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre
