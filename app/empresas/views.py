from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy


# importar la clase generica (Herencia)
from django.views import generic

# Importando el modelo Empresa
# from .models import Empresa
from .models import Business
# Create your views here.



# Esta clase ayuda a paginar todo los datos de la base de datos
class EmpresaListView(generic.ListView):
    model = Business #Referencia al modelo que se va usar.
    context_object_name = 'business_list' #Variabel que hace referencia en el html.
    template_name = "empresas/list.html" #Referencia al html, se mostrar los resultados
    
# Clase generica. Obtiene un objecto por id, checar el archivo urls.py de empresas.
# Esta clase por default hace que "context_object_name" tome el nombre del modelo. 
# En este caso es business, se puede cambiar si lo deseas, solo tienes que especificar como esta arriba
class EmpresaDetail(generic.DetailView):
    model = Business
    template_name='empresas/detail.html'   

# Clase generica. Crea un objeto sin necesidad de un form, checar el archivo urls.py de empresas    
class EmpresaCreateView(generic.CreateView):
    model = Business
    template_name = "empresas/create.html"
    
    # Lista todos los campos que pertenecen al modelo especificado en model
    fields = [
        'nombre',
        'razon_social',
        'direccion',
        'rfc',
        'telefono_oficial',
        'telefono_contacto'
    ]
    success_url = reverse_lazy('empresas:list')#redirecciona a la URL especificada. obtiene la URL correspondiente al nombre especificado como argumento.

# Clase generica. Obtiene un objecto por id, checar el archivo urls.py de empresas.
# El metodo get_context_data() obtiene el context; contexto(data) y los muestra en los campos
class EmpresaUpdateView(generic.UpdateView):
    model = Business
    template_name = "empresas/create.html"
    fields = [
        'nombre',
        'razon_social',
        'direccion',
        'rfc',
        'telefono_oficial',
        'telefono_contacto'
    ]
    success_url = reverse_lazy('empresas:list')
    
    def get_context_data(self, **kwargs):
        context = super(EmpresaUpdateView, self).get_context_data(**kwargs)
        context['update'] = True
        return context

# Clase generica. Obtiene un objecto por id y lo elimina, checar el archivo urls.py de empresas.
class EmpresaDeleteView(generic.DeleteView):
    model = Business
    template_name = "empresas/delete.html"
    success_url = reverse_lazy('empresas:list')





