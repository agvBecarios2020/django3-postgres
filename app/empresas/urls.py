from django.urls import path

from . import views


# Estas rutas se deben incluir en el archivo url del proyecto, ver url de la carpeta app,
# si las rutas no se incluyen, no se puede hacer a ellas.

app_name = 'empresas'

urlpatterns = [
    # Url para las rutas. Cuando se usan clases genericas Detail, Update, y Delete.
    # La url debe ir la primary key como parametro ejemplo "<int:pk>, ver rutas"
    path('', views.EmpresaListView.as_view(), name='list'),
    
    # El path recibe 2 o 3 parametros, segun como quieras usarlo. El primer parametro es
    # la ruta, es decir la direccion. El segundo parametro es la
    # vistas (Las funciones|acciones que se deben hacer) dicha ruta. Tercer y ultimo
    # parametro es la etiqueta (nombre del path) esta etiqueta se usa en los html y vistas
    # para redireccionar
    
    # El views hace referencia al archivo views del proyecto (empresas)
    # con esto puedes acceder a las clases genericas. Despues, se debe
    # especifiar que se usara como vista, segundo parametro.
    path('detail/<int:pk>/', views.EmpresaDetail.as_view(), name='detail'),
    
    path('create/', views.EmpresaCreateView.as_view(), name='create'),
    
    path('edit/<int:pk>/', views.EmpresaUpdateView.as_view(), name='update'),
    
    path('delete/<int:pk>/', views.EmpresaDeleteView.as_view(), name='delete'),
    
]